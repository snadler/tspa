
# Train

TRAIN provides components for a flexible and cross-domain trust infrastructure to sovereignly manage trust anchors with DNS(SEC) and verify the inclusion of entities (e.g. issuers of self-sovereign identity credentials) in trust frameworks.

For more information please visit the website: https://www.hci.iao.fraunhofer.de/de/identity-management/identity-und-accessmanagement/TRAIN_EN.html


# Requirements

* Java 17 or newer
* Maven ~3.6.0
* Internet access 
* TRAIN Trust Infrastructure
* Zone Manager



# How to use it
```shell
  git clone https://gitlab.eclipse.org/eclipse/xfsc/train/tspa.git
  cd tspa/
  mvn clean install
  docker-compose -f docker-compose.yml pull
  docker build -t tspa-service .
  docker-compose -f docker-compose.yml up
```






# Licence
* Apache License 2.0 (see [LICENSE](./LICENSE))
