package eu.xfsc.train.tspa.exceptions;

public class FileExistExceptions extends RuntimeException {
	public FileExistExceptions(String exceptionString) {
		super(exceptionString);
	}
}
