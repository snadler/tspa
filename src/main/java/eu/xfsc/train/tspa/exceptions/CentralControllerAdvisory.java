package eu.xfsc.train.tspa.exceptions;


import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *  Central exception handler.
 */
@ControllerAdvice
public class CentralControllerAdvisory extends ResponseEntityExceptionHandler{
	
	private static final Logger log = LoggerFactory.getLogger(CentralControllerAdvisory.class);
	
	 @ExceptionHandler({PropertiesAccessException.class})
	  protected ResponseEntity<Object> handleConfigurationException(PropertiesAccessException ex) {
		 
		 log.error("PropertiesAccessException, Configuration error : {}", ex.getMessage());
		 Map<String, Object> map = new HashMap<>();
		 map.put("timestamp", LocalDateTime.now());
		 map.put("status", HttpStatus.PRECONDITION_FAILED.value());
		 map.put("error", "Configuration error : "+ ex.getMessage());
	    return new ResponseEntity<>(map, HttpStatus.PRECONDITION_FAILED);
	  }
	 
	 @ExceptionHandler({FileEmptyExceptipon.class})
	  protected ResponseEntity<Object> handleEmptyFileException(FileEmptyExceptipon ex) {
		 
		 log.error("FileEmptyExceptipon, File Empty error : {}", ex.getMessage());
		 Map<String, Object> map = new HashMap<>();
		 map.put("timestamp", LocalDateTime.now());
		 map.put("status", HttpStatus.NOT_FOUND.value());
		 map.put("error", "File Empty error : "+ ex.getMessage());
	    return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
	  }
	 
	 @ExceptionHandler({FileExistExceptions.class})
	  protected ResponseEntity<Object> handleFileExistException(FileExistExceptions ex) {
		 
		 log.error("FileExistExceptions, File Exist error :", ex.getMessage());
		 Map<String, Object> map = new HashMap<>();
		 map.put("timestamp", LocalDateTime.now());
		 map.put("status", HttpStatus.BAD_REQUEST.value());
		 map.put("error", "File already exist  : "+ ex.getMessage());
	    return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
	  }
	 
	 @ExceptionHandler({TSPException.class})
	 protected ResponseEntity<Object> handlTSPExceptions(TSPException ex){	
		 Map<String, Object> map = new HashMap<>();
		 map.put("timestamp", LocalDateTime.now());
		 map.put("status", HttpStatus.BAD_REQUEST.value());
		 map.put("error", ex.getMessage());
		 log.error("TSPException, {}",ex.getMessage());
	    return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
	 }
	 
	 @ExceptionHandler({InvalidStatusCodeException.class})
	 protected ResponseEntity<Object> handlZMExceptions(InvalidStatusCodeException ex){	
		 Map<String, Object> map = new HashMap<>();
		 map.put("timestamp", LocalDateTime.now());
		 map.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
		 map.put("error", ex.getCustomErrorMessage());
		 log.error("InvalidStatusCodeException, {}", ex.getMessage());
	    return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
	 }


}
