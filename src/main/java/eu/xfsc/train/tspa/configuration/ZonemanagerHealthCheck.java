package eu.xfsc.train.tspa.configuration;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class ZonemanagerHealthCheck implements HealthIndicator {

	
	private static final String STATUS = "/status";

	@Value("${Nameserver.Address}")
	private String nameServerAddress;
	@Value("${Nameserver.Token}")
	private String token;

	@Override
	public Health health() {
		if (isZonemanagerHealthy()) {
			return Health.up().withDetail("Status", "Zonemanager is healthy").build();
		} else {
			return Health.down().withDetail("Status", "Zonemanager is not healthy").build();
		}
	}

	private boolean isZonemanagerHealthy() {

		String urlString = nameServerAddress + STATUS;
		HttpClient httpClient = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(urlString)).GET().header("Authorization", "Bearer "+token).build();

		HttpResponse<Void> response = null;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.discarding());
		} catch (IOException | InterruptedException e) {
			return false;
		}
		if (response.statusCode() == 200) {
			return true;
		}
		return false;
	}

}
