package eu.xfsc.train.tspa.services;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import eu.xfsc.train.tspa.exceptions.InvalidStatusCodeException;
import eu.xfsc.train.tspa.interfaces.IZoneManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Service
public class ZoneManager implements IZoneManager {

	private static final Logger log = LoggerFactory.getLogger(ZoneManager.class);

	private static final String NAMES = "names";
	private static final String TRUST_LIST = "trust-list";
	private static final String SCHEMES = "schemes";
	private static final String SEP = "/";
	private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("Application/Json");

	private OkHttpClient mClient = new OkHttpClient();

	@Value("${Nameserver.Address}")
	private String CONFIG_PROPERTY_NAMESERVER;
	
	@Value("${Nameserver.Token}")
	private String CONFIG_PROPERTY_BEARER_TOKEN;
	
	@Value("${Nameserver.query.status}")
	private boolean CONFIG_NS_QUERY_STATUS;


	// Fucntionality for Publishing PTR record in the Zone.
	@Override
	public int publishTrustSchemes(String schemeName, String service) throws IOException, InvalidStatusCodeException {

		String endpoint = buildFullPath(buildSchemeEndpointByServiceName(schemeName));
		log.debug("Zone Manger PUT Endpoint: {}", endpoint);

		@SuppressWarnings("deprecation")
		RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, service);

		Request request = buildRestPutRequest(endpoint, body);
		log.info("Request : {}", request);

		int statusCode = sendRequest(request);
		return statusCode;
	}
	// Function for the Delete the PTR record from the Zone.
	@Override
	public int deleteTrustSchemes(String schemeName) throws IOException, InvalidStatusCodeException {
		String endpoint = buildFullPath(buildSchemeEndpointByServiceName(schemeName));
		log.debug("Zone Manger DELETE Endpoint: {}", endpoint);

		Request request = buildRestDeleteRequest(endpoint);
		log.info("Request : {}", request);

		return sendRequest(request);
	}

	// Function for the publishing URI (DID) record in the Zone.
	@Override
	public int publishDIDUri(String TrustFrameWorkName, String test_list_didString)
			throws IOException, InvalidStatusCodeException {

		String endpoint = buildFullPath(buildTrustListEndpointBySchemeName(TrustFrameWorkName));
		log.info("Endpoint for DID URI: {}", endpoint);

		@SuppressWarnings("deprecation")
		RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, test_list_didString);

		Request request = buildRestPutRequest(endpoint, body);

		int statusCode = sendRequest(request);

		return statusCode;
	}

	// Function for the Deleteing the URI (DID) records form the Zone.
	@Override
	public int deleteDIDUriRecords(String TrustFrameWorkName) throws IOException, InvalidStatusCodeException {
		String endpoint = buildFullPath(buildTrustListEndpointBySchemeName(TrustFrameWorkName));
		log.debug("Zone Manger DELETE Endpoint: {}", endpoint);

		Request request = buildRestDeleteRequest(endpoint);
		log.info("Request : {}", request);

		return sendRequest(request);
	}
	

	/*
	 * Prepare request Endpoint for the Publishing and Deleteing
	 * 
	 */

	private String buildTrustListEndpointBySchemeName(String schemeName) {
		return NAMES + SEP + schemeName + SEP + TRUST_LIST;
	}

	private String buildSchemeEndpointByServiceName(String serviceName) {
		return NAMES + SEP + serviceName + SEP + SCHEMES;
	}

	private String buildFullPath(String endpoint) {
		return getNameServer() + SEP + endpoint;
	}
	

	/*
	 * Configure Zone Manager Token.
	 * 
	 */

	private String getNameServer() {
		if (CONFIG_PROPERTY_NAMESERVER == null) {
			return null;
		}
		return CONFIG_PROPERTY_NAMESERVER;
	}

	private String getBearerToken() {
		return CONFIG_PROPERTY_BEARER_TOKEN;
	}
	

	private Request buildRestPutRequest(String endpoint, RequestBody body) {
		return new Request.Builder().url(endpoint).addHeader("Authorization", "Bearer " + getBearerToken()).put(body)
				.build();
	}

	private Request buildRestDeleteRequest(String endpoint) {

		return new Request.Builder().url(endpoint).addHeader("Authorization", "Bearer " + getBearerToken()).delete()
				.build();
	}

	// Sending the HTTPS request to Zone Manager.
	private int sendRequest(Request request) throws InvalidStatusCodeException, IOException {
		int statusCode = 0;

		try {
			if (CONFIG_NS_QUERY_STATUS) {

				Response response = mClient.newCall(request).execute();
				log.info("Response {} :", response.toString());
				statusCode = response.code();
				log.info("Response Code: {}", statusCode);

				if (statusCode < 200 || statusCode >= 400) {
					log.error("Error during the publication or Deleteing! Wrong status code {} received!", statusCode);
					throw new InvalidStatusCodeException(response.body().string(), statusCode);
				}
			}
		} catch (IOException e) {
			throw e;
		}

		return statusCode;
	}

}
