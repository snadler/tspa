package eu.xfsc.train.tspa.interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.springframework.core.io.Resource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.networknt.schema.ValidationMessage;

import eu.xfsc.train.tspa.exceptions.FileEmptyExceptipon;
import eu.xfsc.train.tspa.exceptions.FileExistExceptions;
import eu.xfsc.train.tspa.exceptions.PropertiesAccessException;
import jakarta.xml.bind.JAXBException;

public interface ITrustListPublicationService {
	
	
	public List<SAXParseException> isXMLValid(String xmlData,Resource schema) throws SAXException, IOException;

	public Set<ValidationMessage> isJSONValid(String jsonData,Resource schema) throws FileNotFoundException, IOException;

	public void initXMLTrustList(String frameworkName, String xml)
			throws FileExistExceptions, FileNotFoundException, PropertiesAccessException;
	
	public void initJsonTrustList(String frameworkName, String xml)
			throws FileExistExceptions, FileNotFoundException, PropertiesAccessException;

	public String getTrustlist(String frameworkName) throws IOException;

	public String deleteTrustlist(String framworkname) throws IOException;

	//TSP
	public void tspPublication(String frameworkName, String tspJson) throws FileEmptyExceptipon, PropertiesAccessException, IOException, JAXBException;

	public void tspRemove(String frameworkName, int uuid) throws FileEmptyExceptipon, PropertiesAccessException, IOException, JAXBException;

	public void tspUpdation(String frameworkName, int uuid, String tspJson) throws FileEmptyExceptipon, PropertiesAccessException, IOException, JAXBException;

}
