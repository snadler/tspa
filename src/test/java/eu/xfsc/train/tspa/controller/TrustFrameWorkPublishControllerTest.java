package eu.xfsc.train.tspa.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Vector;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;

import eu.xfsc.train.tspa.model.json.JsonDid;
import eu.xfsc.train.tspa.model.json.JsonSchemes;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class TrustFrameWorkPublishControllerTest {

	@Autowired
	private MockMvc mvc;

	@Test
	@WithMockUser(authorities = "enrolltf")
	@Order(1)
	public void publishsingleTrustFramework() throws Exception {

		// Arrange
		String pathVariable = "junit-test-gaia-x.testtrain.trust-scheme.de";
		List<String> test_schemes = new Vector<>();
		test_schemes.add("junit-test-gaia-x.testtrain.trust-scheme.de");

		JsonSchemes schemes = new JsonSchemes();
		schemes.setSchemes(test_schemes);

		Gson gson = new Gson();
		String request = gson.toJson(schemes);

		// Act
		mvc.perform(MockMvcRequestBuilders.put("/tspa/v1/trustframework/" + pathVariable)
				.contentType(MediaType.APPLICATION_JSON).content(request))
				// Assert
				.andExpect(status().isCreated());
	}

	@Test
	@WithMockUser(authorities = "enrolltf")
	@Order(2)
	public void publishDidUri() throws Exception {

		// Arrange
		String pathVariable = "junit-test-gaia-x.testtrain.trust-scheme.de";
		String test_did = "did:web:essif.iao.fraunhofer.de";

		JsonDid did = new JsonDid();
		did.setmDid(test_did);

		Gson gson = new Gson();
		String request = gson.toJson(did);

		// Act
		mvc.perform(MockMvcRequestBuilders.put("/tspa/v1/" + pathVariable + "/did")
				.contentType(MediaType.APPLICATION_JSON).content(request))
				// Assert
				.andExpect(status().isCreated());

	}

	@Test
	@WithMockUser(authorities = "enrolltf")
	@Order(3)
	public void deletetrustframework() throws Exception {

		// Arrange
		String pathVariable = "junit-test-gaia-x.testtrain.trust-scheme.de";

		// Act
		mvc.perform(MockMvcRequestBuilders.delete("/tspa/v1/trustframework/" + pathVariable))
				// Assert
				.andExpect(status().isOk());

	}

	@Test
	@WithMockUser(authorities = "enrolltf")
	@Order(4)
	public void deleteTrustListDID() throws Exception {

		// Arrange
		String pathVariable = "junit-test-gaia-x.testtrain.trust-scheme.de";

		//Act
		mvc.perform(MockMvcRequestBuilders.delete("/tspa/v1/" + pathVariable + "/did"))
				// Assert
				.andExpect(status().isOk());
	}

}
